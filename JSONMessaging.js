/// <reference path="RequestReponseIDLDataTypes.ts" />
//
// მეთოდების ჩამონათვალი
//
var JSONMethod;
(function (JSONMethod) {
    JSONMethod[JSONMethod["Reserved0"] = 0] = "Reserved0";
    JSONMethod[JSONMethod["Reserved1"] = 1] = "Reserved1";
    JSONMethod[JSONMethod["Reserved2"] = 2] = "Reserved2";
    JSONMethod[JSONMethod["Reserved3"] = 3] = "Reserved3";
    JSONMethod[JSONMethod["Reserved4"] = 4] = "Reserved4";
    JSONMethod[JSONMethod["Reserved5"] = 5] = "Reserved5";
    JSONMethod[JSONMethod["Reserved6"] = 6] = "Reserved6";
    JSONMethod[JSONMethod["Reserved7"] = 7] = "Reserved7";
    JSONMethod[JSONMethod["Reserved8"] = 8] = "Reserved8";
    JSONMethod[JSONMethod["Reserved9"] = 9] = "Reserved9";
    // კერძო მეთოდების მაგალითები
    // Login = 10,
    // LoginResult = 11 
    JSONMethod[JSONMethod["RegisterUser"] = 10] = "RegisterUser";
    JSONMethod[JSONMethod["RegisterUserResult"] = 11] = "RegisterUserResult";
})(JSONMethod || (JSONMethod = {}));
//
// მეთოდის ტიპების ჩამონათვალი:
//
var JSONMessageType;
(function (JSONMessageType) {
    JSONMessageType[JSONMessageType["Request"] = 0] = "Request";
    JSONMessageType[JSONMessageType["Response"] = 1] = "Response";
    JSONMessageType[JSONMessageType["Notification"] = 2] = "Notification";
})(JSONMessageType || (JSONMessageType = {}));
//
// ტრანზაქციების უზრუნველყოფა
// 
var JSONMessageTransactionState;
(function (JSONMessageTransactionState) {
    JSONMessageTransactionState[JSONMessageTransactionState["Pending"] = 2] = "Pending";
    JSONMessageTransactionState[JSONMessageTransactionState["Failed"] = 3] = "Failed";
    JSONMessageTransactionState[JSONMessageTransactionState["Finished"] = 4] = "Finished";
})(JSONMessageTransactionState || (JSONMessageTransactionState = {}));
//
// სისტემის რანგის შეცდომების, განვრცობადი ჩამონათვალი
// 
var ErrorCode;
(function (ErrorCode) {
    ErrorCode[ErrorCode["None"] = 0] = "None";
    ErrorCode[ErrorCode["UnknowError"] = 1] = "UnknowError";
    ErrorCode[ErrorCode["UnhandledException"] = 2] = "UnhandledException";
    ErrorCode[ErrorCode["Reserved0"] = 3] = "Reserved0";
    ErrorCode[ErrorCode["Reserved1"] = 4] = "Reserved1";
    ErrorCode[ErrorCode["Reserved2"] = 5] = "Reserved2";
    ErrorCode[ErrorCode["Reserved3"] = 6] = "Reserved3";
    ErrorCode[ErrorCode["Reserved4"] = 7] = "Reserved4";
    ErrorCode[ErrorCode["Reserved5"] = 8] = "Reserved5";
    ErrorCode[ErrorCode["Reserved6"] = 9] = "Reserved6";
})(ErrorCode || (ErrorCode = {}));
//
// მაგალითები
//
var Examples;
(function (Examples) {
    //
    // JSON მესიჯის ობიექტის კონკრეტული მაგალითი:
    //
    var loginRequest = {
        method: 10,
        params: {
            userName: "abcd",
            password: "!23ca"
        },
        messageId: 14,
        type: 0,
        errorCode: ErrorCode.None
    };
    var loginResponse = {
        method: 11,
        params: {
            success: true,
            sessionId: "131232kfld"
        },
        messageId: 14,
        type: 1,
        errorCode: ErrorCode.None
    };
    var DocumentationExample;
    (function (DocumentationExample) {
        var _;
        _ = {
            requestMessage: {
                method: JSONMethod.RegisterUser
            },
            responseMessage: {
                method: JSONMethod.RegisterUserResult
            }
        };
    })(DocumentationExample || (DocumentationExample = {}));
})(Examples || (Examples = {}));
